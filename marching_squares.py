import multiprocessing as mp
from values import *

pos = [(0, 0), (1, 0), (1, 1), (0, 1)]
values = [[1] * 100 for i in range(100)]
visited = [[False]*100 for j in range(100)]
curr = [False]


def draw_shape(func, draw_line, domain_to_draw=-1):
    size_sq = 1.0/squares
    curr[0] = not curr[0]

    if domain_to_draw == -1:
        domain_to_draw = [[0, squares, 0, squares]]
    for part in domain_to_draw:
        for i in range(part[0], min(part[1], squares)):
            values[i][part[2]] = func(i * size_sq, part[2] * size_sq)
        for j in range(part[2], min(part[3], squares)):
            values[part[0]][j] = func(part[0] * size_sq, j * size_sq)

        for i in range(part[0], min(part[1], squares)-1):
            for j in range(part[2], min(part[3], squares)-1):
                if visited[i][j] != curr[0]:
                    visited[i][j] = curr[0]
                    values[i+1][j+1] = func((i + 1) * size_sq, (j + 1) * size_sq)
                    pts = []
                    for k in range(4):
                        ind1 = get_indexes(i, j, k)
                        ind2 = get_indexes(i, j, (k+1) % 4)

                        val1 = values[ind1[0]][ind1[1]]
                        val2 = values[ind2[0]][ind2[1]]

                        if val1 * val2 <= 0:
                            pts.append(interpolate(get_point(ind1, size_sq), get_point(ind2, size_sq), val1, val2))

                    if len(pts) == 2:
                        draw_line(pts[0], pts[1])

                    if len(pts) == 4:
                        if func((i + 0.5) * size_sq, (j + 0.5) * size_sq) * values[i][j] <= 0:
                            draw_line(pts[0], pts[1])
                            draw_line(pts[2], pts[3])
                        else:
                            draw_line(pts[0], pts[3])
                            draw_line(pts[1], pts[2])


def get_indexes(i, j, k):
    return i+pos[k][0], j+pos[k][1]


def get_point(ind, size_sq):
    return ind[0]*size_sq, ind[1]*size_sq


def interpolate(pt1, pt2, v1, v2):
    if v1 == 0 and v2 == 0:
        t = 0.5
    else:
        t = abs(v2)/(abs(v1)+abs(v2))
    return [t*x1+(1-t)*x2 for x1, x2 in zip(pt1, pt2)]


def draw_shape_multi(func, draw_line, domain_to_draw=-1):
    #pool = mp.Pool()
    if domain_to_draw == -1:
        domain_to_draw = [[0, squares, 0, squares]]
    domain = range(squares-1)
    processes = []
    results = []
    for part in domain_to_draw:
        for i in range(part[0], min(part[1], squares)-1):
            for j in range(part[2], min(part[3], squares)-1):
                p = mp.Process(target=draw_square, args=(func, i, j, centers, radii, results))
                p.start()
                processes.append(p)
    for p in processes:
        p.join()

    #results = pool.map(draw_square, [(func, i, j, centers, radii) for i in domain for j in domain])
    #pool.close()
    for result in results:
        if result is not None:
            for i in range(0, len(result), 2):
                draw_line(result[i], result[i+1])


#todo: resolve reference issues
def draw_square(func, i, j, c, r, results):
    #func, i, j, c, r, results = args
    size_sq = 1.0/squares
    vals = [[0, 1], [2, 3]]
    '''for k in range(4):
        p = pos[k]
        vals[p[0]][p[1]] = func((i+p[0])*size_sq, (j+p[1])*size_sq)'''
    #print(vals)
    pts = []
    for k in range(4):
        p1 = pos[k]
        p2 = pos[(k + 1) % 4]

        #val1 = values[p1[0]][p1[1]]
        #val2 = values[p2[0]][p2[1]]
        val1 = func((i+p1[0])*size_sq, (j+p1[1])*size_sq, c, r)
        val2 = func((i+p2[0])*size_sq, (j+p2[1])*size_sq, c, r)

        if val1 * val2 <= 0:
            ind1 = get_indexes(i, j, k)
            ind2 = get_indexes(i, j, (k+1) % 4)
            pts.append(interpolate(get_point(ind1, size_sq), get_point(ind2, size_sq), val1, val2))

    if len(pts) == 2:
        results.append([pts[0], pts[1]])

    if len(pts) == 4:
        if func((i + 0.5) * size_sq, (j + 0.5) * size_sq) * values[i][j] <= 0:
            return (pts[0], pts[1]), (pts[2], pts[3])
        else:
            return (pts[0], pts[3]), (pts[1], pts[2])
