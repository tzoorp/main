import pygame
from sys import exit
from dynamic_controll import *
from gpuMarching import *
import numpy as np
from timeit import timeit


# todo: edit while in pause

def draw_line(pt1, pt2):
    res1 = [x * size for x in pt1]
    res2 = [x * size for x in pt2]
    r, g, b, s = 0, 0, 0, 0
    for i in range(len(_centers)):
        w = 1 / ((pt1[0] - _centers[i][0]) ** 2 + (pt1[1] - _centers[i][1]) ** 2) ** .5 if (pt1[0] - _centers[i][
            0]) ** 2 + (pt1[1] - _centers[i][1]) ** 2 != 0 else 100
        r = r + _colors[i][0] * w
        g = g + _colors[i][1] * w
        b = b + _colors[i][2] * w
        s = s + w
    color = pygame.Color(int(r / s), int(g / s), int(b / s))
    pygame.draw.line(_screen, color, res1, res2)


def draw_domain(domain):
    for part in domain:
        pt1 = (part[0] / 100, part[2] / 100)
        pt2 = (part[0] / 100, part[3] / 100)
        pt3 = (part[1] / 100, part[2] / 100)
        pt4 = (part[1] / 100, part[3] / 100)
        draw_line(pt1, pt2)
        draw_line(pt2, pt4)
        draw_line(pt4, pt3)
        draw_line(pt3, pt1)


def draw(squares):
    values = np.zeros((squares, squares), np.float64)
    visited = np.zeros((squares, squares), bool)
    lines = draw_shape_j(values, visited, squares, _centers, _radii, get_domain(_centers, _radii, dom_r, squares))
    for line in lines:
        draw_line(line[0], line[1])


if __name__ == '__main__':

    _centers = np.array([[0.5, 0.5], [0.2, 0.2], [0.2, 0.3], [0.4, 0.6], [0.5, 0.7]])
    _radii = np.array([0.1, 0.12, 0.15, 0.1, 0.08])
    _velocities = np.array([[0.3, 0.1], [0.2, 0.3], [0.1, 0.3], [-0.2, 0.15], [0.1, 0.15]])
    _colors = np.array([[255, 0, 0], [0, 255, 0], [0, 0, 255], [190, 100, 70], [255, 0, 255]])
    paused = False
    fps = 60
    _dt = 1.0 / fps
    size = 600
    _squares = 100
    dom_r = 0.85

    _screen = pygame.display.set_mode((size, size))
    pygame.init()
    pygame.display.set_caption('Balls')
    clock = pygame.time.Clock()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    paused = not paused
                    if paused:
                        _screen.fill("Black")
                        draw(2*_squares)
                if event.key == pygame.K_LEFT:
                    dt = -1.0 / fps
                if event.key == pygame.K_RIGHT:
                    dt = 1.0 / fps

        if not paused:
            _screen.fill("Black")
            draw(_squares)
            update_dynamics(_centers, _velocities, _radii, _dt)

        pygame.display.update()
        clock.tick(fps)
