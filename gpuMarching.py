from numba import njit, cuda
import numpy as np
from dynamic_controll import meta_ball


@njit(target_backend=cuda)
def draw_shape_j(values, visited, squares, centers, radii, domain_to_draw):
    curr = True
    size_sq = 1.0/squares
    lines = []
    for part in domain_to_draw:
        for i in range(part[0], min(part[1], squares)):
            values[i][part[2]] = meta_ball(i * size_sq, part[2] * size_sq, centers, radii)
        for j in range(part[2], min(part[3], squares)):
            values[part[0]][j] = meta_ball(part[0] * size_sq, j * size_sq, centers, radii)

        for i in range(part[0], min(part[1], squares)-1):
            for j in range(part[2], min(part[3], squares)-1):
                if visited[i][j] != curr:
                    visited[i][j] = curr
                    values[i+1][j+1] = meta_ball((i + 1) * size_sq, (j + 1) * size_sq, centers, radii)
                    pts = []
                    for k in range(4):
                        ind1 = get_indexes(i, j, k)
                        ind2 = get_indexes(i, j, (k+1) % 4)

                        val1 = values[ind1[0]][ind1[1]]
                        val2 = values[ind2[0]][ind2[1]]

                        if val1 * val2 <= 0:
                            pts.append(interpolate(get_point(ind1, size_sq), get_point(ind2, size_sq), val1, val2))

                    if len(pts) == 2:
                        lines.append((pts[0], pts[1]))

                    if len(pts) == 4:
                        if meta_ball((i + 0.5) * size_sq, (j + 0.5) * size_sq, centers, radii) * values[i][j] <= 0:
                            lines.append((pts[0], pts[1]))
                            lines.append((pts[2], pts[3]))
                        else:
                            lines.append((pts[0], pts[3]))
                            lines.append((pts[1], pts[2]))
    return lines


@njit(target_backend=cuda)
def get_indexes(i, j, k):
    pos = np.array([(0, 0), (1, 0), (1, 1), (0, 1)])
    return i+pos[k][0], j+pos[k][1]


@njit(target_backend=cuda)
def get_point(ind, size_sq):
    return ind[0]*size_sq, ind[1]*size_sq


@njit(target_backend=cuda)
def interpolate(pt1, pt2, v1, v2):
    if v1 == 0 and v2 == 0:
        t = 0.5
    else:
        t = abs(v2)/(abs(v1)+abs(v2))
    return [t*x1+(1-t)*x2 for x1, x2 in zip(pt1, pt2)]
