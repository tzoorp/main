from numba import njit, cuda
import numpy as np


@njit(target_backend=cuda)
def meta_ball(x, y, centers, radii):
    f = 0
    for i in range(len(radii)):
        a, b = centers[i]
        d = ((x-a)*(x-a)+(y-b)*(y-b))**0.5
        f = f + radii[i]/d-1 if d != 0 else 0
    return f


def get_domain(centers, radii, dom_r, squares):
    domain_to_draw = []
    for i in range(len(radii)):
        c = centers[i]
        rad = radii[i]
        domain_to_draw.append((int((c[0] - dom_r * rad) * squares), int((c[0] + dom_r * rad) * squares),
                               int((c[1] - dom_r * rad) * squares), int((c[1] + dom_r * rad) * squares)))
    return np.array(domain_to_draw, dtype=int)


#todo linear interpolation for collision detection
@njit(target_backend=cuda)
def update_dynamics(centers, velocities, radii, dt):
    for i in range(len(radii)):
        center = centers[i]
        v = velocities[i]
        rad = radii[i]
        center[0] += v[0] * dt
        center[1] += v[1] * dt

        if center[1] <= rad or center[1] >= (1 - rad):
            v[1] = -v[1]
        if center[0] <= rad or center[0] >= (1 - rad):
            v[0] = -v[0]

